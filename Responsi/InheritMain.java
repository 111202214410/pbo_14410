import java.util.Scanner;

public class InheritMain {
    public static void main(String[] args) {

        Scanner scn = new Scanner(System.in);
        System.out.println("Program Hitung Pembayaran");
        System.out.println("========================================");
        System.out.println("Nama : ");
        String nama = scn.nextLine();
        System.out.println("Alamat : ");
        String address = scn.nextLine();
        System.out.println("Nim : ");
        String nim = scn.nextLine();

        Student student = new Student(nama, address, nim);
        System.out.println("Masukan SPP : ");
        int spp = scn.nextInt();
        student.setSpp(spp);
        System.out.println("Masukan Sks : ");
        int sks = scn.nextInt();
        student.setSks(sks);
        System.out.println("Masukan Modul : ");
        int modul = scn.nextInt();
        student.setModul(modul);

        int total = student.hitungPembayaran(spp, sks, modul);

        System.out.println("Total tagihan pembayaran : " + total);
        Person person = new Person(nama, address);
        person.setHobi("Main Badminton");
        person.hobi();
        scn.close();
    }
}

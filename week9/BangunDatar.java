package week9;

public class BangunDatar {
    protected int panjang;
    protected int lebar;
    protected double diameter;
    protected int sisi;
    protected double jari;
    protected double alas;
    protected double tinggi;
    protected float half;
    protected static final double pi = 3.14;

    public BangunDatar(int sisi) {
        this.sisi = sisi;
    }

    public BangunDatar(int panjang, int lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public BangunDatar(double jari) {
        this.jari = jari;
    }

    public BangunDatar(double alas, double tinggi) {
        this.tinggi = tinggi;
        this.alas = alas;
    }

    public int kelPersegi() {
        return 4 * sisi;
    }

    public int luasPersegi() {
        return sisi * sisi;
    }

    public int kelPerPan() {
        return 2 * (panjang + lebar);
    }

    public int luasPerPan() {
        return lebar * panjang;
    }

    public double kelLingkar() {
        return pi * (2 * jari);
    }

    public double luasLingkar() {
        return pi * jari * jari;
    }

    public int getSisi() {
        return sisi;
    }

    public int getPanjang() {
        return panjang;
    }

    public int getAlas() {
        return alas;
    }

    public int getTinggi() {
        return tinggi;
    }

    public int getLebar() {
        return lebar;
    }

    public double getDiameter() {
        return diameter;

    }

    public double luasSegitiga() {
        return alas * tinggi / 2;
    }

    public double kelSegitiga() {
        return 3 * alas;
    }
}
